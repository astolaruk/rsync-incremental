# rsync-incremental
This is a script I commonly use to perform incremental backups on *nix systems.

As stated in the script itself, it is inspired by the indespensable tutorial at https://blog.interlinked.org/tutorials/rsync_time_machine.html.

#!/bin/bash

# Be sure to install mailutils if you want email notifications
# Based on the amazing tutorial at https://blog.interlinked.org/tutorials/rsync_time_machine.html

# Remember to create a backup.include file before running

# Sample backup.include
#+ home
#+ home/user
#- home/user/backup/local
#- home/user/.ssh
#+ home/user/**
#+ etc
#+ etc/nginx
#+ etc/nginx/**
#- *

### BASIC SETTINGS
	# Cosmetic name for this backup job
	backupname="rsync-incremental"
	# Where the the local backups should be stored
	workdir=$HOME/backup
	# Location of your backup.include file
	backupinclude=$workdir/backup.include

### NOTIFICATION SETTINGS
	# Set to 1 to enable email notifications
	sendemail=0
	# Email address to send notifications to
	adminemail="root@localhost"
	# Email address to send notifications from
	fromemail="root@localhost"

### REMOTE BACKUPS
	# Set to 1 to enable remote backup
	do_remote_backup=0
	# Server to send remote backups to (if using)
	remoteserver="user@localhost"
	# Locaton of backups on the remote server (default: same as local)
	remotepath=$workdir

### ADVANCED SETTINGS
	# Date format to display in emails and logs (default ISO 8601)
	emaildate=`date "+%Y-%m-%dT%H:%M:%S"`
	# Where the logfiles should be created
	logpath=$workdir/logs
	# What the logfile should be named
	logname="backup.$date.txt"

### BACKUP RETENTION MODE
	# By default, create a new backup everytime this script is run.
	# Backups must be pruned manually.
	date=`date "+%Y-%m-%dT%H:%M:%S"`
	# Only keep backups for the last 7 days (Monday-1, Tuesday-2, ...)
	#date=`date "+%w-%A"

# Output process exit code and check for errors
errorcheck()
{
        exitcode=$(echo $?)
        echo "" >> $logfile
        echo "RSYNC EXIT CODE: $exitcode" >> $logfile
        echo "" >> $logfile

        # If a non-zero exit code is returned, append ERROR to the subject line
        if [[ $exitcode -ne 0 ]] && [[ ! $backupname == *"ERRORS"* ]]
        then
                backupname="ERRORS - $backupname"
        fi
}

# Check to see if the backup directory structure is created, if not then create it
	if [ ! -d $workdir ]; then mkdir -p $workdir; fi
	if [ ! -d $workdir/logs ]; then mkdir $workdir/logs; fi
	if [ ! -d $workdir/local ]; then mkdir $workdir/local; fi

# Create a new log file
	logfile=$logpath/$logname
	/bin/echo "Backup: $backupname back-$date" > $logfile
	/bin/echo "" >> $logfile

### LOCAL BACKUP

# Do the local backup
	/bin/echo "--- LOCAL BACKUP ---" >> $logfile
	/bin/echo "" >> $logfile

	rsync -avhP --include-from $backupinclude --link-dest=$workdir/local/current / $workdir/local/back-$date 2>> $logfile >> $logfile
	errorcheck
	rm -f $workdir/local/current
	ln -s back-$date $workdir/local/current

### REMOTE BACKUP

if [ $do_remote_backup -eq 1 ]; then
	/bin/echo "" >> $logfile
	/bin/echo "--- REMOTE BACKUP ---" >> $logfile
	/bin/echo "" >> $logfile

	# Be sure that the target directory exists on the remote server
	ssh $remoteserver "mkdir -p $remotepath"

	# Transfer the remote backup
	rsync -avhP --include-from $backupinclude --link-dest=$remotepath/current / $remoteserver:$remotepath/back-$date 2>> $logfile >> $logfile
	errorcheck
	ssh $remoteserver "rm -rf $remotepath/current"
	ssh $remoteserver "ln -s $remotepath/back-$date $remotepath/current"
fi

# Show the job duration
	duration=$SECONDS
	echo "DURATION: $(($duration / 60 / 60))h $(($duration / 60 % 60))m $(($duration % 60))s" >> $logfile

# Send the notification email
if [ $sendemail -eq 1 ]; then
	#Email log file to admin
	/bin/echo | /bin/cat $logfile | mail -s "$backupname Backup Job" -a "From: $fromemail" "$adminemail"
fi
